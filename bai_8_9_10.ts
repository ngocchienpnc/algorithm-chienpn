
function LinearSearch1(arr: number[], id: number) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === id) {
            return i;
        }
    }
    return -1;
}
let arr = [5, 1, 3, 7, 11, 6, 8, 0, 4, 2, 9, 10];

console.log('--Linear Search--');

let search = LinearSearch1(arr, 5)
console.log(search);

function bubbleSort(arr: number[],) {

    for (var i = 0; i < arr.length - 1; i++) {
        for (var j = 0; j < arr.length - 1; j++) {
            if (arr[j] > arr[i]) {
                let temp = arr[j]
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    return arr;
}

console.log('--BubbleSort--');
let bubble = [9, 2, 5, 6, 0, 1, 3, 4, 8, 7]
let searchBubble = bubbleSort(arr)
console.log('Mảng chưa sắp xếp');
console.log(arr);
console.log('Mảng đã được sắp xếp');
console.log(searchBubble);

function insertionSort(arr: number[]) {


    for (let i = 0; i < arr.length; i++) {
        let key = arr[i];
        let j = i - 1;

        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = key;
    }
    return arr;
}

console.log('--Insertion Sort--');
let insertion = [7, 2, 4, 1, 5, 6, 9, 8, 3]

console.log('Mảng chưa sắp xếp');
console.log(insertion);

console.log('Mảng đã được sắp xếp');

let insertionSortResult = insertionSort(insertion)
console.log(insertionSort);

function selectionSort(arr: number[]): number[] {

    for (let i = 0; i < arr.length; i++) {

        let minIndex = i;

        for (let j = i + 1; j < arr.length; j++) {

            if (arr[j] < arr[minIndex]) {
                minIndex = j
            }
        }
        if (minIndex !== i) {
            let temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
    }
    return arr;
}

console.log('--Selection Sort--');
let selection = [22, 18, 10, 23, 11, 18, 19, 27, 14,]

console.log('Mảng chưa sắp xếp');
console.log(selection);
let selectionsort = selectionSort(selection);

console.log('Mảng đã được sắp xếp');
console.log(selectionsort);