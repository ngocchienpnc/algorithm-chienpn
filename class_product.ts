
class Product {
    id: number;
    productName: string;
    categoryId: number;
    saleDate: Date;
    qulity: number;
    isDelete: boolean;
    constructor(id: number, name: string, categoryId: number, saleDate: Date, qulity: number, isDelete: boolean){
        this.id = id;
        this.productName = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.qulity = qulity;
        this.isDelete = isDelete
    }

}

const productList :Product[]=[
    {id: 11, productName:"Product 11", categoryId: 1011, saleDate:new Date(2024,0,5), qulity:1, isDelete:true},
    {id: 12, productName:"Product 12", categoryId: 1012, saleDate:new Date(2024,0,5), qulity:2, isDelete:false},
    {id: 13, productName:"Product 13", categoryId: 1013, saleDate:new Date(2024,0,7), qulity:1, isDelete:true},
    {id: 14, productName:"Product 14", categoryId: 1014, saleDate:new Date(2024,0,8), qulity:2, isDelete:false},
    {id: 15, productName:"Product 15", categoryId: 1015, saleDate:new Date(2024,0,4), qulity:2, isDelete:true},
    {id: 16, productName:"Product 16", categoryId: 1016, saleDate:new Date(2024,0,22), qulity:1, isDelete:false},
    {id: 17, productName:"Product 17", categoryId: 1017, saleDate:new Date(2024,0,22), qulity:1, isDelete:true},

]

//Bài 11
function fiterProductByID(productList: Product[], productID:number){
    for (const product of productList) {
        if (product.id === productID) {
            
            
            return product.productName;
        }
               
    }
    return false;
}

let productIdToFind = 12;
let productName = fiterProductByID(productList, productIdToFind);
if (productName !== undefined) {
    console.log(`Product Name with ID ${productIdToFind}: ${productName}`);
} else {
    console.log(`Product with ID ${productIdToFind} not found.`);
}

//----TypeScript---
function filterProductByIdUseTS(listProduct: Product[], idProduct: number): string | undefined {
    const foundProduct = listProduct.find(product => product.id === idProduct);
    return foundProduct?.productName;
}

let productID = 13;
let NameProduct = filterProductByIdUseTS(productList,productID)
if (NameProduct !== undefined) {
    console.log(`Product Name with ID ${productID}: ${NameProduct}`);
} else {
    console.log(`Product with ID ${productID} not found.`);
}


// bài 12
function fiterProductByQulity(productList: Product[]){
    let productListResut : Product[] =[]

    for(const product of productList){
        if (product.qulity > 0 && !product.isDelete) {
            productListResut .push(product)
        }
    }

    return productListResut;
}
let fiterProduct = fiterProductByQulity(productList);
console.log('--------Arr Product có qulity > 0 và chưa bị xóa------------------- ');
console.log('---1----');

console.log(fiterProduct);


//-----TypeScript--------------------------------
function fiterProductByQulityUseTS(productList: Product[]){
    return productList.filter(product => product.qulity > 0 && !product.isDelete);
}

let fiterProduct2 = fiterProductByQulityUseTS(productList)

console.log('---2---');
console.log(fiterProduct2);


// Bài 13----------------------

function fiterProductBySaleDate(listProduct: Product[]){
    let date = new Date()
    let productSaleDate : Product[] = [];

    for(const product of listProduct){
        if(product.saleDate > date && !product.isDelete){
            return productSaleDate.push(product);
    }
    return productSaleDate;
}
}

let fiterProductSaleDate = fiterProductBySaleDate(productList);

console.log("Sản phẩm sale date:");
console.log("----1----");
console.log(fiterProductSaleDate);


function fiterProductBySaleDateUseTS(productList: Product[]){
    let date = new Date();
    return productList.filter(product => product.saleDate > date && !product.isDelete)
}

let fiterProductSaleDate2 = fiterProductBySaleDateUseTS(productList);
console.log('----2----');
console.log(fiterProductSaleDate2);


// bài 14 tính tổng

function totalProduct(productList: Product[]){
    let total = 0;
    for(const product of productList){
        if(!product.isDelete){
            total +=product.qulity;
        }
    }
    return total;
}

let totalQulity = totalProduct(productList);

console.log("totalQulity: " + totalQulity);


//---TypeScript-----------------------
function totalProductUseTS(productList: Product[]){
    return productList
    .filter(product => !product.isDelete)
    .reduce((total, product)=> total+product.qulity,0);
}
let totalQulity2 = totalProductUseTS(productList);

console.log("totalQulity2: " + totalQulity2);


// Bài 15 
function isHaveProductInCategory(productList: Product[], categoryId: number){
    let productInCategory: boolean= false;
    for (const product of productList){
        if (categoryId === product.categoryId){
            productInCategory = true;
        }
    }
    return productInCategory;
}

let productInCategory = isHaveProductInCategory(productList,1012)
console.log('Is Have Product In Category: ' + productInCategory);


function isHaveProductInCategoryUseTS(productList: Product[], categoryId: number){
    return productList.some(product => product.categoryId === categoryId)
}

let productInCategory2 = isHaveProductInCategoryUseTS(productList,116)
console.log('Is Have Product In Category: ' +productInCategory2);


// Bài 16

function fiterProductBySale(productList: Product[]){
    let date = new Date();
    let productSale: any[]=[]
    for(const product of productList){
        if(product.saleDate > date && product.qulity > 0){
            productSale.push([product.id, product.productName]);
        }
    }
    return productSale
}

let fiterProductSale = fiterProductBySale(productList);
console.log("Product sales:");
console.log(fiterProductSale);
//---TypeScript---------------------------------
function fiterProductBySaleUseTS(productList: Product[]){

    let date = new Date();

    return productList.filter(product => product.saleDate > date && product.qulity >0).map(({id, productName: nameProduct}) =>([id, nameProduct]))
}

let fiterProductSaleUseTs = fiterProductBySaleUseTS(productList);
console.log("Product sales:");
console.log(fiterProductSaleUseTs);
