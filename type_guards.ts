type anphanumeric = string | number;

function add(a: anphanumeric,b: anphanumeric){
    let c: number;
    if(typeof a==="number" && typeof b==="number"){
        c = a + b;

        return console.log(`${a} + ${b} = ${c}`);
        
    }
    if(typeof a==="string" && typeof b==="string"){

        return console.log(`${a} - ${b}`);
    }
    throw new Error("Invalid argument. Both arguments must be either numbers or strings")
}

console.log(add(1,2));


class Customer1{
    isCreditAllowed(): boolean{
        return true;
    }
}
class Supplier{
    isInShortList(): boolean{
        return true;
    }
}

type Partner = Customer1 | Supplier;



function signContract(partner: Partner) : string {
    let message: string = '';
    if (partner instanceof Customer1) {
        message = partner.isCreditAllowed() ? 'Sign a new contract with the customer' : 'Credit issue';
    }

    if (partner instanceof Supplier) {
        message = partner.isInShortList() ? 'Sign a new contract the supplier' : 'Need to evaluate further';
    }

    return message;
}


