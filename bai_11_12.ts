class ProductA{
    name: string;
    price: number;
    quality: number;
    categoryId: number;
    constructor (name: string, price: number, quality: number, categoryId: number){
        this.name = name;
        this.price = price;
        this.quality = quality;
        this.categoryId = categoryId;
    }
}

let producta: ProductA[] = [
    {name : 'CPU', price: 750, quality: 10, categoryId: 1},
    {name : 'RAM', price: 50, quality: 2, categoryId: 2},
    {name : 'HDD', price: 70, quality: 1, categoryId: 2},
    {name : 'Main', price: 400, quality: 3, categoryId: 1},
    {name : 'Keyboard', price: 30, quality: 8, categoryId: 4},
    {name : 'Mouse', price: 25, quality: 50, categoryId: 4},
    {name : 'VGA', price: 60, quality: 35, categoryId: 3},
    {name : 'Monitor', price: 120, quality: 28, categoryId: 2},
    {name : 'Case', price: 120, quality: 28, categoryId: 5}
]

console.log(producta);

//Bai 11
function sortByPrice(product: ProductA[]){

    for(var i=0; i<product.length -1; i++){
        for(var j=0; j < product.length - 1 - i; j++){
            if (product[j].price > product[j+1].price){ 
               let temp = product[j]
               product[j] = product[j + 1];
               product[j + 1] = temp;
            }
        }
    }
    return product;
   
}
let bubblesort = sortByPrice(producta)
console.log(bubblesort);


// bai 12
function sortByName(product: ProductA[]){

   
    for(let i = 0; i < product.length; i++){
        let key = product[i];
        let j = i-1;

        while(j>= 0 && product[j].name.length > key.name.length) {
            product[j+1] = product[j];
        j--;
        }
        product[j +1] = key;
    }
    return product;
}

let sortName = sortByName(producta)
console.log(sortName);