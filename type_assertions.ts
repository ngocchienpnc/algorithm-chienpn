function getNetPrice(price: number, discount: number, format: boolean): number | string {
    let netPrice= price * (1-discount);
    return format ? `${netPrice}`: netPrice;
}

// console.log(getNetPrice(1000,0.05, false));

let netPrice = getNetPrice(2000,0.5,true) as string;

console.log(netPrice);

let netPrice2 = <number>getNetPrice(1000,0.4,false);


