interface BusinessPartner{
    name: string,
    credit: number,
}

interface Identity{
    id: number,
    name: string,
}
interface Contact{
    email: string,
    phone: string,
}

type Employee = Identity & Contact;

let e: Employee = {
    id: 1,
    name: "John",
    email: "john@example.com",
    phone: "123"
}

type Customer= Identity & BusinessPartner & Contact;

let c: Customer= {
    id: 2,
    name: "John",
    credit: 123,
    email: "john@",
    phone: "123"

}
