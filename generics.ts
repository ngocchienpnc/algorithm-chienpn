function getRandomNumberElement(items: number[]): number {
    let randomIndex = Math.floor(Math.random() * items.length);

    return items[randomIndex];
}
let numbers = [1, 5, 7, 4, 2, 9];
console.log(getRandomNumberElement(numbers));


function getRandomStringElement(items: string[]): string {
    let randomIndex = Math.floor(Math.random() * items.length);

    return items[randomIndex];
}

let strings=['a', 'b', 'c', 'd', 'e'];
console.log(getRandomStringElement(strings));


function getRandomAnyElement(items: any[]): any{
    let randomIndex = Math.floor(Math.random() *items.length);

    return items[randomIndex];

}

console.log(getRandomAnyElement(numbers));
console.log(getRandomAnyElement(strings));


function getRandomElement<T>(items: T[]): T {
    let randomIndex = Math.floor(Math.random() * items.length);
    return items[randomIndex];
}

let randomEle = getRandomElement<number>(numbers); 
let randomEle1 = getRandomElement<string>(strings);
console.log(randomEle1);


//Generic Constraints

function merge<U, V>(obj1: U, obj2: V) {
    return {
        ...obj1,
        ...obj2
    };
}
let result = merge(
    { name: 'John' },
    { jobTitle: 'Developer' }
);
console.log(result);

function merge1<U extends object, V extends object>(obj1: U, obj2: V) {
    return {
        ...obj1,
        ...obj2
    };
}
let person = merge1(
    { name: 'John' },
    {age: 25}
);

console.log(person);


function prop<T, K extends keyof T>(obj: T, key: K) {
    return obj[key];
}

let str = prop({ name: 'John' }, 'name');
console.log(str);

//Generic Interfaces
interface Pair<K, V> {
    key: K;
    value: V;
}
let month : Pair<string, number>={
    key: 'Jan',
    value: 1
}

console.log(month);

interface Collection<T> {
    add(o: T): void;
    remove(o: T): void;
}

class List<T> implements Collection<T>{
    private items: T[] = [];

    add(o: T): void {
        this.items.push(o);
    }
    remove(o: T): void {
        let index = this.items.indexOf(o);
        if (index > -1) {
            this.items.splice(index, 1);
        }
    }
}

let list = new List<number>();

for (let i = 0; i < 10; i++) {
    list.add(i);
}

console.log(list);
